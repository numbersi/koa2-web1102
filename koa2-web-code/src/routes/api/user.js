
const router = require('koa-router')();
const { isExist, register, login, deleteCurUser } = require('../../controller/user.js');
const userValidate = require('../../validator/user.js');      
const genValidator = require('../../middlewares/validator.js')
const { loginCheck } = require('../../middlewares/loginChecks.js')
const { isTest } = require('../../utils/env.js');

router.prefix('/api/user');

//注册
router.post('/register', genValidator(userValidate) ,async (ctx) => {
  const { userName, password, gender } = ctx.request.body;
  ctx.body = await register({userName, password, gender})
})


//登录
router.post('/login',genValidator(userValidate),async (ctx) => {
  const { userName, password } = ctx.request.body;
  ctx.body = await login(ctx,userName,password);
})


//判断用户名是否存在
router.post('/isExist',async (ctx) => {
  const {userName} = ctx.request.body;
  ctx.body = await isExist(userName)
})

// 测试环境下删除用户
router.post('/delete',loginCheck, async (ctx) => {
  if (isTest) {
    const { userName } =ctx.session.userInfo;
    ctx.body = await deleteCurUser(userName)
  }
})


module.exports = router;