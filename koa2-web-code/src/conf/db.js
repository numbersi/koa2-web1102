
//数据库配置
const { isDev, isProd, isTest } = require('../utils/env.js');
let REDIS_CONF = {
  host: '127.0.0.1',
  port: 6379,
  family: 4,   //ipv4 
};
let MYSQL_CONF ={
  config:{
    host: '120.76.61.132',
    port: 3306,
    dialect: 'mysql',
  },
  database: 'weiboTest',
  user: 'weiboTest',
  password: '123456'  
}

if (isDev) { 
  REDIS_CONF = {
    host: '127.0.0.1',
    port: 6379,
    family: 4,   //ipv4 
  } 
} else if (isTest) {
  MYSQL_CONF.config.logging = () => {}    //测试环境，不打印sql语句
} else if (isProd) {
  MYSQL_CONF.config.pool = {
    max: 5,     //连接池的最大连接数
    min: 0,
    idle: 10000,  //如果一个连接池10s没有被使用，则释放
  }
}

module.exports = {
  REDIS_CONF,
  MYSQL_CONF
}