module.exports = {
    "parser":"babel-eslint",
    "env": {
        "browser": true,
        "es6": true,
        "commonjs": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "indent":["error",2],
        "quotes":[
            "error",
            "single",
            {
                "allowTemplateLiterals": true
            }
        ],
        "require-atomic-updates": "off"
    }
};