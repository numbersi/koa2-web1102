
function add (a,b) {
  return a+b;
}

function getAuther(userInfo) {
  return userInfo
}

function getIntArray(num) {
  if (!Number.isInteger(num)){
    //console.error('请输入正整数');   //这样写--toThrow无法检测
    throw Error('请输入正整数');
  }
  const resultArra =[];
  for(let i =0; i<num; i++) {
    resultArra.push(i)
  }

  return resultArra
}

//toBe基本类型比较
test(' 1 add 2 equal 3 ',() => {
  expect(add(1,2)).toBe(3)      
})


//toEqual引用类型比较
test('getAuther返回的对象属性和深度相等', ()=>{
  expect(getAuther({name:'zyj',age:27})).toEqual(getAuther({name:'zyj',age:27}))
})


//toHaveLength字符串和数组长度是否满足预期
test('字符串和数组的长度是否满足预期',() => {
  expect(getIntArray(3)).toHaveLength(3)
})


//toThrow 被测试函数是否满足预期抛出异常(必须使用一个函数对被测试函数做一个包装)
test('getIntArray(3.3)应该抛出异常',() => {
  expect(() => {
    getIntArray(3.3)
  }).toThrow()
})

//toMatch --正则匹配
test('getAuther.name应该包含zyj',() => {
  expect(getAuther({name:'zyj',age:27}).name).toMatch(/zyj/ig)
})




