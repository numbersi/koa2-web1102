


// class SuccessModel extends BaseModel {
//   constructor(data = {}) {
//     super();                               //子类是没有this的--必须通过super继承父类的
//     this.errno = 0;                        //这样写不行哦,errno是父类已有的，只能继承，不能重新定义吗？？
//     this.data =data;
//   }
// }



//----------------------------------------------------------


class BaseModel {
  constructor({errno, data, message}) {    //构造函数的私有属性--详见react-共享单车 -es6知识拓展
    this.errno = errno
    if (data) {
      this.data = data
    }
    if (message) {
      this.message = message
    }
  }
}

/**
* 成功的数据模型
*/
class SuccessModel extends BaseModel {
  constructor(data = {}) {
    super({
      errno: 0,
      data
    })
  }
}

/**
* 失败的数据模型
*/
class ErrorModel extends BaseModel {
  constructor({ errno, message }) {
    super({
      errno,
      message
    })
  }
}



module.exports = {
  SuccessModel,
  ErrorModel
}


//疑问---继承父类的意义何在？？？
// 还不如  
// class SuccessModel {
//   constructor (data = {}) {
//     this.errno = 0;
//     this.data = data;
//   }
// }
