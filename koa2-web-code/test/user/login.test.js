

const server = require('../server.js');

const userName = `u_${Date.now()}`;
const password = `p_${Date.now()}`;

const testUser = {
  userName,
  password,
  gender: 1
}

let COOKIE = '';

//--------------注册---------------
test('注册一个用户,应该成功' , async () => {
  const res = await server.post('/api/user/register').send(testUser);
  expect(res.body.errno).toBe(0)
});

test('重复注册用户应该失败', async () => {
  const res = await server.post('/api/user/register').send(testUser);
  expect(res.body.errno).not.toBe(0)
})

test('查询注册的用户应该存在', async () => {
  const res = await server.post('/api/user/isExist').send({userName});
  expect(res.body.errno).toBe(0)
})

test('json schema 检查非法的注册数据格式,应该失败', async () => {
  const res = await server.post('/api/user/register').send({
    userName:'123',
    password: '1',
    gender:'男'
  });
  expect(res.body.errno).not.toBe(0)
})



//------------登录-----------------
test('登录应该成功', async () => {
  const res = await server.post('/api/user/login').send({userName,password});
  expect(res.body.errno).toBe(0);
  //获取cookie
  COOKIE = res.headers['set-cookie'].join(';');
})

//---删除用户---
test('删除当前用户，应该成功', async () => {
  const res = await server
      .post('/api/user/delete')
      .set('cookie', COOKIE)
  expect(res.body.errno).toBe(0)
})



