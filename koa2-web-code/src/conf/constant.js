


module.exports = {
  DEFAULT_PICTURE: 'https://dwz.cn/rnTnftZs',    //用户头像默认地址
  PAGE_SIZE: 5,

  // 正则表达式，匹配 '@昵称 - userName'
  REG_FOR_AT_WHO: /@(.+?)\s-\s(\w+?)\b/g
}