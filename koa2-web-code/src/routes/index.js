const router = require('koa-router')()
// const { loginCheck, loginRedirect } = require('../middlewares/loginChecks.js')
router.get('/', async (ctx) => {
  await ctx.render('index', {
    title: 'Hello 微博项目!',
    index: '首页',
    isMe: true,
    blogList:[{id:1,title:'aaa'},{id:2,title:'bbb'},{id:3,title:'ccc'},]
  })
})

router.get('/string', async (ctx) => {
  ctx.body = 'koa2 string supertest'
})


router.get('/json', async (ctx) => {
  ctx.body = {
    title: 'koa2 json'
  }
})

module.exports = router


