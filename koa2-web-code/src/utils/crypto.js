
const CRYPTO_SECRET_KEY = 'SD123ui_sd$@';

const crypto = require('crypto');   //nodejs自带核心模块(加密算法)

function _md5(content) {
  const hash = crypto.createHash('md5');
  return hash.update(content).digest('hex');     //输出16进制的字符串

}

function doCryPto(content) {
  const str = `password=${content}$key=${CRYPTO_SECRET_KEY}`;
  return _md5(str)
}

module.exports = doCryPto;

