
const router =require('koa-router')();

function getLoginInfo(ctx) {
  const { userInfo } = ctx.session;
  const loginInfoData = {
    isLogin: false,
    userInfo
  }
  if (userInfo) {
    return {
      ...loginInfoData,
      isLogin: true
    }
  } else {
    return loginInfoData 
  }
}

router.get('/login',async (ctx) => {
  await ctx.render('login',getLoginInfo(ctx))
})

router.get('/register',async (ctx) => {
  await ctx.render('register',getLoginInfo(ctx))
})

module.exports = router;