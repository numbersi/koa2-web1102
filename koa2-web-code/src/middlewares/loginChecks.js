

const { ErrorModel } = require('../model/ResModel.js');
const { loginCheckFailInfo } = require('../model/errorInfo.js');
//-- api 接口权限验证
const loginCheck = async (ctx,next) => {
  if (ctx.session && ctx.session.userInfo) {
    await next()
  } else {
    ctx.body =  new ErrorModel(loginCheckFailInfo)
  }
}


//页面跳转权限验证
const loginRedirect = async (ctx,next) => {
  if (ctx.session && ctx.session.userInfo) {
    await next()
  } else {
    const curUrl =  encodeURIComponent(ctx.url); 
    ctx.redirect(`/login?${curUrl}`)
  }
}

module.exports = {
  loginCheck,
  loginRedirect
}
