// sync.js ---- 同步模型

const seq = require('./seq.js')

require('./model/index.js')  //引入要同步的模型


//测试连接
seq.authenticate()
  .then(() => {
    console.log('weiboTest connect successful')
  })
  .catch((err) => {
    console.log('unable to connect to weiboTest',err)
  })

//执行同步[一次同步所有的模型](同步完成后退出sequelize进程)  force: true
seq.sync({ alert:true }).then(() => {
  console.log('sync success');
}).then(() => {
  process.exit();   //nodejs 退出程序
})
