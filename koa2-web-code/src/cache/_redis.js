


//redis连接

const { REDIS_CONF } = require('../conf/db.js');
const Redis = require('ioredis');
const redisClient = new Redis(REDIS_CONF);

const setRedis = (key,val,expire) => {
  if (typeof val === 'object') {
    val = JSON.stringify(val)
  }
  if (expire) {
    redisClient.set(key,val,'EX',expire);
  } else {
    redisClient.set(key,val)
  }
}

const getRedis = (key,successCallBack) => {
  redisClient.get(key).then((result) => {
    try{
      successCallBack(JSON.parse(result))
    }catch(err){
      successCallBack(result)
    } 
  })
}

const delRedis = async (key) => {
  if (key) {
    await redisClient.del(key)
  } else {
    await redisClient.flushdb()    //删除当前数据库中的所有key     //flushall() 删除所有数据库中所有的key
  }
  
}

module.exports = {
  setRedis,
  getRedis,
  delRedis
}
