
// 数据验证函数，作为中间件放在 api 接口中
//校验方法返回结果都一样,知识json-schema数据不一样
const { ErrorModel } = require('../model/ResModel.js');

function genValidator (validateFn) {
  const  validatorFn = async (ctx,next) => {
    const validateResult =  validateFn(ctx.request.body) 
    if (!validateResult){
      await next()
    } else {
      ctx.body = new ErrorModel({
        errno: 10009,
        message: validateResult
      })
    }
  } 
  return validatorFn
}

module.exports = genValidator;