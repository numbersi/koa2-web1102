

const {  DEFAULT_PICTURE } = require('../conf/constant.js')

function _formatUserPicture(obj) {
  if (!obj.picture) {
    obj.picture = DEFAULT_PICTURE
  } 
  return obj
}


function formatUser(list) {
  if (list instanceof Array) {
    return list.map(val => _formatUserPicture(val))
  } else if (list instanceof Object) {
    return _formatUserPicture(list)
  }
}

module.exports = {
  formatUser
}


