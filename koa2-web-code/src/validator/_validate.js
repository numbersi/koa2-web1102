


const Ajv = require('ajv');
const ajv = new Ajv({
  // allErrors: true // 输出所有的错误（比较慢）
});

function validate(schema,data = {}) {
  const validate = ajv.validate(schema,data);
  if (!validate){
    console.log('--validate--errors:',ajv.errors);
    const { dataPath,message } = ajv.errors[0];
    return  `${dataPath}-${message}` 
  } else {
    return null
  }
}


module.exports = validate;




