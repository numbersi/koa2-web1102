
const Sequelize = require('sequelize');

module.exports = {
  INTEGER: Sequelize.INTEGER,
  FLOAT:  Sequelize.FLOAT,
  STRING: Sequelize.STRING,
  DECIMAL: Sequelize.DECIMAL,
  TEXT: Sequelize.TEXT,
  DATE: Sequelize.DATE,
  BOOLEAN: Sequelize.BOOLEAN
}